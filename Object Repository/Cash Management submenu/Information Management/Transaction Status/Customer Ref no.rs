<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Customer Ref no</name>
   <tag></tag>
   <elementGuidId>68fd3991-f459-4658-95f1-9a9ec4d10cc7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@name=&quot;TransactionStatusActionForm&quot;]/table[3]/tbody/tr[3]/td[2]/input[@name=&quot;orderPartyRefNo&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Parent iframe/Parent_mainframe</value>
   </webElementProperties>
</WebElementEntity>
