<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a5bcfc97-b7d2-4805-82c8-b2581e15a961</testSuiteGuid>
   <testCaseLink>
      <guid>62a65540-70ca-4341-8aa9-c5998ccdda17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00a60f27-b72b-48f1-81cc-9a25cbefe161</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8377825f-cecd-46e5-bb10-dbb507e6c7f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/3. Domestic Online Transfer/Domestic Online Transfer (Future)/TC 3.5B.3A New Entry Local or Forex/TC 3.5B.3A.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34816936-fc0d-4a87-ad43-d0e05dcf2373</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/3. Domestic Online Transfer/Domestic Online Transfer (Future)/TC 3.5B.3B Predefined Local or Forex/TC 3.5B.3B.1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
