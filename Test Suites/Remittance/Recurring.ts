<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Recurring</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f6a94ef6-a1ef-4362-b8f8-5ffc1691e79d</testSuiteGuid>
   <testCaseLink>
      <guid>204acfc0-4cd6-4746-afd8-c5c634f83f32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>051d82d4-ec53-43f1-be4a-6b23c57595c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d90d90b8-58f6-4672-a04f-0dbc94fb5c2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Recurring)/TC 3.5.6M New Entry Local - Forex Counter/TC 3.5.6M.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dd22828-e04d-40d7-ba3e-08d07d4c73bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Recurring)/TC 3.5.6N Predefined Local - Forex Counter/TC 3.5.6N.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b20d9ad-b713-4ac8-a580-8fc9a050b212</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Recurring)/TC 3.5.6O Predefined Forex - Forex Counter/TC 3.5.6O.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
