import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Bulk Payment Management/Bulk Payment/Bulk payment page'))

WebUI.delay(2)

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Bulk Payment Management/Bulk Payment/file format'), 'CSV', 
    false)

WebUI.click(findTestObject(null))

WebUI.uploadFile(findTestObject('Cash Management submenu/Bulk Payment Management/Bulk Payment/file upload'), '')

WebUI.click(findTestObject(null))

WebUI.click(findTestObject('Cash Management submenu/Bulk Payment Management/Bulk Payment/source account'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Source Account IDR 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.click(findTestObject(null))

WebUI.click(findTestObject('Cash Management submenu/Bulk Payment Management/Bulk Payment/future radio'))

WebUI.setText(findTestObject('Cash Management submenu/Bulk Payment Management/Bulk Payment/future - set date'), '')

WebUI.click(findTestObject('Cash Management submenu/Bulk Payment Management/Bulk Payment/confirm'))

