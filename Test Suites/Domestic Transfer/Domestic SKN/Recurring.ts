<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Recurring</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4b692dd2-3558-4060-b5ca-3887a54fceda</testSuiteGuid>
   <testCaseLink>
      <guid>0a148800-774c-4681-bfc1-e84ca264778a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>492e4baf-3e87-452a-b558-91f6915fbaba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>085ca2e8-43e8-470c-9812-13ea2a7c2eff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Recurring)/TC 3.5C.2A Predefined Forex - Local Counter/TC 3.5C.2A.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b3d7f55-abff-4c23-9116-9e88a1503463</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Recurring)/TC 3.5C.2B New Entry Forex - Local Counter/TC 3.5B.2B.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98676d73-49c5-4bc7-b593-3fe2ec866990</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Recurring)/TC 3.5C.2C Predefined Local - Local Counter/TC 3.5C.2C.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
