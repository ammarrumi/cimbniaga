<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>dd196bb5-cdd4-40f9-ae6c-4788fd86c4d8</testSuiteGuid>
   <testCaseLink>
      <guid>867a45aa-8616-4263-8951-9b8a8f36fa81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9cb8b8b-b402-481e-bd6a-71d7874a487b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7fff274-1e12-4567-945a-44330e69a6e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Future)/TC 3.5.6I New Entry Local - Forex Murex Deal/TC 3.5.6I.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d992bea7-f39f-44d3-b35f-c39994dc62ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Future)/TC 3.5.6J New Entry Local - Forex Forex Deal/TC 3.5.6J.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a403ec5c-3921-4a08-a586-bcc3894dce46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Future)/TC 3.5.6K New Entry Forex - Forex Counter/TC 3.5.6K.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
