<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6eb883fd-213c-486a-87da-a46a35045964</testSuiteGuid>
   <testCaseLink>
      <guid>3b575ef2-d464-4e56-8cef-d475387bc47d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5c023ed-2d4d-48e2-9210-dfe3c6f6424f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec35d330-6970-407a-8b70-4eefd94a4f7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Future)/TC 3.5B.1A Other CIMB Local - Forex Counter/TC.3.5B.1A</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcc06520-af44-4143-8c55-ccf584c15cf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Future)/TC 3.5B.1B Other CIMB Local - Forex Murex/TC 3.5B.1B.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bd25aee-8940-4093-babe-f7bfa50d59c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Future)/TC 3.5B.1C Other CIMB Local - Local Counter/TC 3.5B.1C.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c1a16db-de09-4b12-8368-7825cd3aa688</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Future)/TC 3.5B.1D Other CIMB Forex - Local Counter/TC 3.5B.1D.1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
