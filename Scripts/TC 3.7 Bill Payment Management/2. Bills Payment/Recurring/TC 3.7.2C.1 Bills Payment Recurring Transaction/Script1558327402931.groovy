import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/Bills payment page'))

WebUI.delay(2)

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/Source account'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Source Account IDR 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/payee radio'))

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/payee search'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/payee name pln'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring radio'))

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring - frequency'), 'Daily', 
    false)

not_run: WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring - frequency'), 
    'Weekly', false)

not_run: WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring - frequency'), 
    'Monthly', false)

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring - interval(daily)'), 
    '0')

not_run: WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring - interval(weekly)'), 
    '0')

not_run: WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring - interval(monthly)'), 
    '0')

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring - session'), '0')

WebUI.setText(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/recurring - end date'), '')

not_run: WebUI.setText(findTestObject(null), '')

not_run: WebUI.click(findTestObject(null))

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bills Payment/continue'))

