<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fbe8e70f-737a-4f1e-b9cb-3b8395a5c02d</testSuiteGuid>
   <testCaseLink>
      <guid>69a9574d-88a9-4082-95b1-f64a445b702a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b922e91-5974-43b1-a6c5-62f639d2cfa8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18da6cc3-c5c3-4bdd-a29e-966ee90d5cc8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Future)/TC 3.5B.2A New Entry Forex - Local Murex/TC 3.5B.2A.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7306be4-182d-49e9-b5d1-b7cd9d513d83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Future)/TC 3.5B.2B Predefined Forex - Local Forex Deal/TC 3.5B.2B.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29bce862-af78-46b5-a5e3-539449678549</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Future)/TC 3.5B.2C New Entry Local - Local Counter/TC 3.5B.2C.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
