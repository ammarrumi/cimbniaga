<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>File Format</name>
   <tag></tag>
   <elementGuidId>e3947cb8-37e6-4349-8a44-ea44bfa21773</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@name=&quot;BillPaymentUploadActionForm&quot;]/table[3]/tbody/tr[5]/td[2]/select[@name=&quot;fileChoose&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Parent iframe/Parent_mainframe</value>
   </webElementProperties>
</WebElementEntity>
