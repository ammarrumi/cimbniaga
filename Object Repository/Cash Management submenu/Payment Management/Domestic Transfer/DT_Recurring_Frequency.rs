<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DT_Recurring_Frequency</name>
   <tag></tag>
   <elementGuidId>0eb7d81d-237d-4213-9936-26fca98a94c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@name=&quot;ft&quot;]/table[12]/tbody/tr[6]/td[2]/select</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Parent iframe/Parent_mainframe</value>
   </webElementProperties>
</WebElementEntity>
