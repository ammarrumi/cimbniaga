<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Recurring</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5e04e5d3-c277-405b-8748-07c43a5a8472</testSuiteGuid>
   <testCaseLink>
      <guid>5c923a80-5b3c-4a3e-9e44-8e98cd58096a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f4e7889-05eb-4e6b-8229-fdde37c233c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32b9082e-1a13-4820-aede-7053091c0416</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Recurring)/TC 3.5C.1A Registered Local - Forex/TC 3.5C.1A.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97368a9d-4517-4968-ad61-3360688a31e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Recurring)/TC 3.5C.1B Registered Local - Forex/TC 3.5C.1B.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8beb7cb5-7d15-4a1c-a7b7-12e8bc7eb220</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Recurring)/TC 3.5C.1C Predefined Local - Forex/TC 3.5C.1C.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8bd43d03-0614-433d-9e85-80fae33752de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Recurring)/TC 3.5C.1D Registered Forex - Local/TC 3.5C.1D.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ef65916-84fb-4206-a9de-a6433991e1cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Recurring)/TC 3.5C.1E Registered Forex - Local/TC 3.5C.1E.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abce30a0-d76a-4893-ad4c-ff8b23a01b55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Recurring)/TC 3.5C.1F Registered Local - Local/TC 3.5C.1F.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aae637e7-e9d6-4634-a317-dad7cc10395e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Recurring)/TC 3.5C.1G Predefined Local - Local/TC 3.5C.1G.1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
