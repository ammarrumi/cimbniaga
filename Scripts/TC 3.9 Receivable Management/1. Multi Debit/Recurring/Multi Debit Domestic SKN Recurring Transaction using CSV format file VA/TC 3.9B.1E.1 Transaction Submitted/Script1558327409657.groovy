import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/multi debit page'))

WebUI.delay(2)

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/file format'), 'CSV', 
    false)

WebUI.click(findTestObject(null))

WebUI.uploadFile(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/file upload'), '')

WebUI.click(findTestObject(null))

WebUI.click(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/domestic radio'))

WebUI.click(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/domestic - virtual radio'))

WebUI.click(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/benef account domestic'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject(null))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.setText(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/total record'), '')

WebUI.setText(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/total amount'), '')

WebUI.setText(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/purpose domestic'), '')

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/transaction type domestic'), 
    '', false)

WebUI.click(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring radio'))

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring - frequency'), 
    'Daily', false)

not_run: WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring - frequency'), 
    'Weekly', false)

not_run: WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring - frequency'), 
    'Monthly', false)

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring - interval daily'), 
    '0')

not_run: WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring - interval weekly'), 
    '0')

not_run: WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring - interval monthly'), 
    '0')

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring - session'), 
    null)

WebUI.setText(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/recurring - end date'), '', FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject(null))

WebUI.click(findTestObject('Cash Management submenu/Receivable Management/Multi Debit/confirm'))

