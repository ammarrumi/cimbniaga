<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>In-House Transfer_Save As</name>
   <tag></tag>
   <elementGuidId>fd245e37-ff41-4107-911d-f5b467cfcf1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@name=&quot;InHouseTransferActionForm&quot;]/table[5]/tbody/tr/td/input[@name=&quot;Save&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Parent iframe/Parent_mainframe</value>
   </webElementProperties>
</WebElementEntity>
