<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Recurring</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5ca01e2b-7384-4e37-a536-7842ddfa4b74</testSuiteGuid>
   <testCaseLink>
      <guid>0a55672d-4620-4edd-8ec5-d256b39ebef2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a3090fa-abc5-4522-8442-ded5b03547c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2f5edb4-1290-4a03-8a73-fe5ec0aadeed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/3. Domestic Online Transfer/Domestic Online Transfer (Recurring)/TC 3.5C.3A New Entry Local or Forex/TC 3.5C.3A.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67bd3698-7e7e-4fec-9fb7-5a946b312091</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/3. Domestic Online Transfer/Domestic Online Transfer (Recurring)/TC 3.5C.3B Predefined Local or Forex/TC 3.5C.3B.1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
