<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Information Management Page</name>
   <tag></tag>
   <elementGuidId>64bed8e0-17b5-45d6-9b9e-29d507a5fb42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[text()=&quot;
												    Information Management&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Parent iframe/Parent_menuframe</value>
   </webElementProperties>
</WebElementEntity>
