import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Show Payment Management submenu'))

WebUI.verifyElementPresent(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/Display In-House Transfer Page'), 
    0)

WebUI.verifyElementPresent(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/Display Domestic Transfer Page'), 
    0)

WebUI.verifyElementPresent(findTestObject('Cash Management submenu/Payment Management/Domestic Online Transfer/Display Domestic Online Transfer Page'), 
    0)

WebUI.verifyElementPresent(findTestObject('Cash Management submenu/Payment Management/Remittance/Display remittance page'), 
    0)

WebUI.verifyElementPresent(findTestObject('Cash Management submenu/Payment Management/Predefined Beneficiary List/Display Predefined Beneficiary List Page'), 
    0)

