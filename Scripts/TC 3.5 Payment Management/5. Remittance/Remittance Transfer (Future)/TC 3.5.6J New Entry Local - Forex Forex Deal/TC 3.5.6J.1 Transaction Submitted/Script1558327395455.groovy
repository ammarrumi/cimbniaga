import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Remittance/Display remittance page'))

WebUI.delay(5)

WebUI.verifyElementPresent(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Source Acc (Search)'), 
    0)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Source Acc (Search)'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Source Account IDR 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Benef Acc_New Entry (Radio)'))

WebUI.delay(2)

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Benef Acc_Acc number'), '5552000')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Benef Acc_Acc Name'), 'UMET')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Benef Acc_Acc Address'), 'SOLO')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Benef Acc_Bank name'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Rmtnc Bank 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Amount_ccy'), 'USD', 
    false)

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Amount_Total'), '10')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Exc Rate_Special Rate (Radio)'))

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Exc Rate_Reference'), '')

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_LHBU_Purpose'), '2')

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_LHBU_Docu type'), '2')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Future Payment_Radio'))

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Future Payment_Session'), 
    '1')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Future Payment_Date (fill)'), '28/09/2018')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Remittance/RE_Confirm'))

