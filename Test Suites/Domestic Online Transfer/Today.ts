<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Today</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>dedc2b2a-c411-4dec-b86c-57c07637b00c</testSuiteGuid>
   <testCaseLink>
      <guid>6c7e7ffb-04d1-4e90-9ed7-c084f3bb50ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a59dff64-1a08-4026-a302-27e89a27a791</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>594c805e-563a-4174-a401-f427c2ed58ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/3. Domestic Online Transfer/Domestic Online Transfer (Today)/TC 3.5A.3A New Entry Local or Forex/TC 3.5A.3A.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12164eb5-288a-48a8-9432-9e619b138568</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/3. Domestic Online Transfer/Domestic Online Transfer (Today)/TC 3.5A.3C Predefined Local or Forex/TC 3.5A.3C.1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
