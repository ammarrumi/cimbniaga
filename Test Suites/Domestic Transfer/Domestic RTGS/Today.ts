<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Today</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>603cf0f7-6077-414f-8533-322ba52daabd</testSuiteGuid>
   <testCaseLink>
      <guid>b5eeb312-e05f-4879-b448-3327434b375f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a44f785-b05c-461d-95d4-b462b3270358</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5294fe15-8a08-41ba-b273-950770d0a505</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/TC 3.5D.2A New Entry Forex - Local Murex/TC 3.5D.2A.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75f8b990-c3d8-4315-85e8-93ca5ccfc29e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/TC 3.5D.2B New Entry Forex - Local Forex Deal/TC 3.5D.2B.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4961c5b-12d7-4676-8b16-65f246c332f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/TC 3.5D.2C New Entry Local - Local Counter/TC 3.5D.2C.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8bd9e18-f5dd-4f23-928b-3eb2ebeff49e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/TC 3.5D.2D Predefined Forex - Local Murex/TC 3.5D.2D.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f5111af-3d74-4b5b-9d2a-7f6ce13448fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/TC 3.5D.2E Predefined Forex - Local Forex Deal/TC 3.5D.2E.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>928d76c1-3156-410c-baf5-b86f8581d2ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/TC 3.5D.2F Predefined Local - Local Counter/TC 3.5D.2F.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
