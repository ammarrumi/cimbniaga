<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Today</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6d6f4553-329d-4811-a265-9a45685a844c</testSuiteGuid>
   <testCaseLink>
      <guid>6ced2279-2a37-4608-a1ff-a9316f6faa09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c7ed6e0-730a-4e6c-921c-8f6622f61f46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b088770-c975-4a1c-9f6c-b5991ff31101</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.1A Other CIMB Local - Forex Counter/TC 3.5A.1A.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e5aec31-8fbc-45b5-afac-cdbda7a61c88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.1B Other CIMB Local - Forex Murex/TC 3.5A.1B.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66275468-5fac-41fa-b7f1-b98c991e0289</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.1C Other CIMB Local - Forex Murex/TC 3.5A.1C.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cb0344b-70d4-4f17-8dec-e0308b7e98b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.1D Other CIMB Local - Forex Forex/TC 3.5A.1D.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>637d738f-d97a-4d83-a4ca-6ead0f701667</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.1E Other CIMB Local - Local Counter/TC 3.5A.1E.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d5931d6-8b46-4cae-8840-0bf8f5532a63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.1F Other CIMB Forex - Local Counter/TC 3.5A.1F.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b577edb-fc44-4920-8c63-5061fcbea374</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.1G Other CIMB Forex - Local Counter/TC 3.5A.1G.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58455ead-8611-40eb-a847-1e7f0d77458d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.2B Registered Local - Forex Counter/TC 3.5A.2B.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acc84f02-4b11-4c89-a026-e542fe07653e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.2C Registered Local - Forex Murex/TC 3.5A.2C.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>907e3854-ea8b-40f5-ae99-efad6447501e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.2D Registered Local - Forex Murex/TC 3.5A.2D.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed16f8b7-5fe7-47f3-ab5a-2af9151e5000</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.2F Registered Local - Local Counter/TC 3.5A.2F.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b47adb31-98d0-46e7-af09-4b1340867541</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.2G Registered Forex - Local Counter/TC 3.5A.2G.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe14d7b4-d445-4713-ba25-09ad9dab4519</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/1. In-House Transfer/In-House Transfer (Today)/TC 3.5A.2H Registered Forex - Local Counter/TC 3.5A.2H.1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
