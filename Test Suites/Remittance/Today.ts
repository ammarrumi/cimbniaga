<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Today</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>27dbcd55-f46e-40b5-9a4c-434d3366eecf</testSuiteGuid>
   <testCaseLink>
      <guid>1a0ee510-d9d3-45ef-852b-6723ebf25eaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef5cf847-7a24-4f06-94da-b3ef0a0a4d57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>315e911e-8a92-46b7-ba03-6df5f414b7c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Today)/TC 3.5.6A New Entry Local - Forex Murex Deal/TC 3.5.6A.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7928b736-c341-43af-9210-ae1c497954eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Today)/TC 3.5.6B New Entry Local - Forex Forex Deal/TC 3.5.6B.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efa429cc-b7c7-4541-a0d8-7b53c9d182fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Today)/TC 3.5.6C New Entry Forex - Forex Counter/TC 3.5.6C.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19a10342-f85b-4e09-aa2c-de4ba77ce474</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Today)/TC 3.5.6E Predefined Local - Forex Murex Deal/TC 3.5.6E.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34ba9c97-4663-46ed-afdf-15dae7fea6a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Today)/TC 3.5.6F Predefined Local - Forex Forex Deal/TC 3.5.6F.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a4c6755-3855-466e-a79a-9cba7096d671</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/5. Remittance/Remittance Transfer (Today)/TC 3.5.6G Predefined Forex - Forex Counter/TC 3.5.6G.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
