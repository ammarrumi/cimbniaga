import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/Display In-House Transfer Page'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Source Account_Search'), 
    0)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Source Account_Search'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Source Account IDR 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Benef Acc_Other CIMB_Fill'), 
    '700000085300')

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Amount_ccy'), 
    'IDR', false)

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Amount_totalamount'), 
    '149100')

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Purpose'), 
    2)

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Docu Type'), 
    2)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Future Payment Date(Radio)'))

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Future Payment_Session Time'), 
    '1')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Future Payment Date(Fill)'), 
    '28/09/2018')

WebUI.delay(5)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/In-House Transfer/In-House Transfer_Confirm Button'))

