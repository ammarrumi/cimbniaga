<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5aab8623-0763-4c52-9036-27e4916d918a</testSuiteGuid>
   <testCaseLink>
      <guid>e09be2de-0b81-48a5-92ec-3a32ed767a52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aeb813c4-6d65-46bb-bd06-8d9d2a89f3ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0023d101-40c3-40c5-8c5a-6096bcee9890</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Future)/TC 3.5E.2A New Entry Forex - Local Murex/TC 3.5E.2A.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84205645-5eb9-4f75-a30b-d36a69fffaa0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Future)/TC 3.5E.2B Predefined Forex - Local Counter/TC 3.5E.2B.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c8bafce-1249-46e0-a08d-039ce61c4795</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Future)/TC 3.5E.2C New Entry Local - Local Counter/TC 3.5E.2C.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
