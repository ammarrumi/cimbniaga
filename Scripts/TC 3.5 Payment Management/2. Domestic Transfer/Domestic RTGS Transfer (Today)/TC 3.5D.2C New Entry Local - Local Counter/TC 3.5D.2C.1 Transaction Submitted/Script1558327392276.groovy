import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/Display Domestic Transfer Page'))

WebUI.delay(5)

WebUI.verifyElementPresent(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Source Acc_Search'), 
    0)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Source Acc_Search'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Source Account IDR 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Benef Acc_New Entry (Radio)'))

WebUI.delay(2)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Benef Acc_New Entry_Bank Name'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/DT Bank 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Benef Acc_New Entry_Acc No'), 
    '5550000')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Benef Acc_New Entry_Acc Name'), 
    'UMET')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Benef Acc_Amount'), '200000')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Benef Acc_RTGS Local - Local (Radio)'))

WebUI.delay(5)

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Benef Acc_Address'), 'TES')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Benef Acc_City District'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/City 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Domestic Transfer/DT_Confirm Local - Local'))

