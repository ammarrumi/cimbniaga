import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Cash Delivery Page'))

WebUI.delay(2)

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Frequency'), 'Ad Hoc', 
    false)

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Pick up date'), '')

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Amount - Currency'), 
    'IDR', false)

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Amount - Total'), '')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Source Account'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Source Account IDR 1'))

WebUI.delay(5)

WebUI.switchToWindowUrl('http://10.25.143.28:7001/corp/common2/login2.do?action=login2')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/CP Name'), 'Fajar')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/CP Phone'), '080988821')

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Delivery Mode'), 'Pick Up at Branch', 
    false)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Branch'))

WebUI.switchToWindowTitle('Corporate Internet Banking')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Branch(1)'))

WebUI.delay(0)

WebUI.switchToWindowUrl('')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Deliv Time'), '09:00')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Recipient Name'), 'Fahmi')

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Recipient ID'), '6969')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Confirm'))

WebUI.delay(5)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Cash Delivery/Submit'))

