<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Today</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c2c8562f-ec7a-45d4-9296-9787a4bf2d2d</testSuiteGuid>
   <testCaseLink>
      <guid>95a8edf7-55b7-4706-851e-d247046f2c99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cf74942-1414-45fb-a103-45153b20ab9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f0e9c75-fc34-4086-933c-8e8a03db2dd3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/Domestic SKN Transfer (Today)/TC 3.5A.2A New Entry Forex - Local Murex/TC 3.5A.2A.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9e3ec1b-bb6b-4ff6-99b9-d2b880d3fc80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/Domestic SKN Transfer (Today)/TC 3.5A.2B New Entry Forex - Local Forex Deal/TC 3.5A.2B.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea178cfd-ed9c-4f55-a505-531464678df7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Today)/Domestic SKN Transfer (Today)/TC 3.5A.2C New Entry Local - Local Counter/TC 3.5A.2C.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe7d197c-2044-426b-9faf-a13d04a58534</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Today)/TC 3.5A.2D Predefined Forex - Local Murex/TC 3.5A.2D.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8bf1c65-ee2e-4f47-a7ed-14ff8151b2ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Today)/TC 3.5A.2E Predefined Forex - Local Forex Deal/TC 3.5A.2E.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1847825-43df-4274-8526-088e0d0e57c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic SKN Transfer (Today)/TC 3.5A.2F Predefined Local - Local Counter/TC 3.5A.2F.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
