import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Information Management/Transaction Status/Transaction Status Page'))

WebUI.delay(2)

not_run: WebUI.click(findTestObject('Cash Management submenu/Information Management/Transaction Status/Transaction Ref no radio'))

not_run: WebUI.setText(findTestObject('Cash Management submenu/Information Management/Transaction Status/Transaction Ref no'), 
    '')

not_run: WebUI.click(findTestObject('Cash Management submenu/Information Management/Transaction Status/Customer Ref no radio'))

not_run: WebUI.setText(findTestObject('Cash Management submenu/Information Management/Transaction Status/Customer Ref no'), 
    '')

WebUI.click(findTestObject('Cash Management submenu/Information Management/Transaction Status/Date Range radio'))

WebUI.click(findTestObject('Cash Management submenu/Information Management/Transaction Status/Created date radio'))

not_run: WebUI.click(findTestObject('Cash Management submenu/Information Management/Transaction Status/Instruction date radio'))

WebUI.setText(findTestObject('Cash Management submenu/Information Management/Transaction Status/Date from'), '')

WebUI.setText(findTestObject('Cash Management submenu/Information Management/Transaction Status/Date to'), '')

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Information Management/Transaction Status/Menu'), 'Bill Payment Upload', 
    false)

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Information Management/Transaction Status/instruction mode'), 
    'Standing Instruction Transfer', false)

WebUI.click(findTestObject('Cash Management submenu/Information Management/Transaction Status/Search'))

