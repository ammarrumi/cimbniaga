import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('http://10.25.143.28:7001/corp/common2/login.do?action=loginRequest')

WebUI.maximizeWindow()

WebUI.waitForPageLoad(0)

WebUI.verifyElementPresent(findTestObject('Login - Landing Page/Company ID'), 0)

WebUI.verifyElementPresent(findTestObject('Login - Landing Page/User'), 0)

WebUI.setText(findTestObject('Login - Landing Page/Company ID'), 'biz000110dum')

WebUI.setText(findTestObject('Login - Landing Page/User'), 'singleuser01')

WebUI.setText(findTestObject('Login - Landing Page/verif'), 'e')

WebUI.click(findTestObject('Login - Landing Page/Login - Continue'))

WebUI.setText(findTestObject('Login - Landing Page/Password'), 'password1')

WebUI.click(findTestObject('Login - Landing Page/Login - Continue'))

WebUI.waitForPageLoad(0)

WebUI.verifyElementPresent(findTestObject('Login - Landing Page/Main page verify'), 0)

