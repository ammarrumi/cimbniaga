<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Confirm</name>
   <tag></tag>
   <elementGuidId>e49c330b-eb02-4b97-9242-cc17fcdf5b40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@name=&quot;BillPaymentUploadActionForm&quot;]/table[7]/tbody/tr/td/input[@name=&quot;continueButton&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Parent iframe/Parent_mainframe</value>
   </webElementProperties>
</WebElementEntity>
