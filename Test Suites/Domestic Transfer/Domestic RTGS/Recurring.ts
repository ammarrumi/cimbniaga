<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Recurring</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>29d17465-7f74-4209-95b3-0f45d71f62f8</testSuiteGuid>
   <testCaseLink>
      <guid>4def8b1f-56d6-4c13-8fc1-90b8cf63556a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93519ada-355d-4969-824c-ff4d268dffc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Show Payment Management Submenu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3a15d38-8934-4c36-953e-95d53821f6cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Recurring)/TC 3.5F.2A New Entry Forex - Local Counter/TC 3.5F.2A.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fab5941-43fa-4b94-81ac-9050d0940158</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Recurring)/TC 3.5F.2B Predefined Forex - Local Counter/TC 3.5F.2B.1 Transaction Submitted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d4d7a36-7404-45f0-bd24-bf5305c7fed5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC 3.5 Payment Management/2. Domestic Transfer/Domestic RTGS Transfer (Recurring)/TC 3.5F.2C Predefined Local - Local Counter/TC 3.5F.2C.1 Transaction Submitted</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
