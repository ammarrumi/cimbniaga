import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('TC 3.1B Login as Corporate User  - Maker/TC 3.1B.1.1 singleuser'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Show Payment Management submenu'))

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Predefined Beneficiary List/Display Predefined Beneficiary List Page'))

WebUI.delay(2)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Predefined Beneficiary List/PBL_Online/PBL_Online display'))

WebUI.delay(2)

WebUI.setText(findTestObject('Cash Management submenu/Payment Management/Predefined Beneficiary List/PBL_Online/PBL_Online_Unique Name'), 
    'BABON')

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Payment Management/Predefined Beneficiary List/PBL_Online/PBL_Online_Bank (list)'), 
    '0')

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Predefined Beneficiary List/PBL_Online/PBL_Online_Search (button)'))

WebUI.delay(10)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Predefined Beneficiary List/PBL_Online/PBL_Online_Delete (button)'))

WebUI.delay(2)

WebUI.click(findTestObject('Cash Management submenu/Payment Management/Predefined Beneficiary List/PBL_Online/PBL_Online_Delete_Submit (button)'))

