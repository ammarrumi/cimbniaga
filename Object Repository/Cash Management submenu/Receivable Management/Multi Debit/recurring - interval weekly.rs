<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>recurring - interval weekly</name>
   <tag></tag>
   <elementGuidId>e04ccdb8-ab04-4df4-b381-93bc9a1c39fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@name=&quot;AutoDebetActionForm&quot;]/div[@id=&quot;standingInstruction&quot;]/div[@id=&quot;recurring&quot;]/table/tbody/tr[3]/td[2]/div[@id=&quot;dayDiv&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Parent iframe/Parent_mainframe</value>
   </webElementProperties>
</WebElementEntity>
