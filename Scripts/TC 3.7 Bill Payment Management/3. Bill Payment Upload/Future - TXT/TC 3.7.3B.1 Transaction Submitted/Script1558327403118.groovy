import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/Bill Payment Upload page'))

WebUI.delay(2)

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/Institution Category'), 
    '1', FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/Institution'), '1', FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/File Format'), 'TXT', 
    false)

not_run: WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/File Type'))

WebUI.uploadFile(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/File Upload'), '')

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/Future - Radio'))

WebUI.setText(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/Future - Date'), '')

WebUI.selectOptionByIndex(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/Future - Session'), '0')

WebUI.click(findTestObject('Cash Management submenu/Bill Payment/Bill Payment Upload/Confirm'))

