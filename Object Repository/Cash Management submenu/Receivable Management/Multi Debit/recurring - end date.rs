<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>recurring - end date</name>
   <tag></tag>
   <elementGuidId>721ee5c2-8c08-4963-8c08-93f53f7b68ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@name=&quot;AutoDebetActionForm&quot;]/div[@id=&quot;standingInstruction&quot;]/div[@id=&quot;recurring&quot;]/table/tbody/tr[5]/td[2]/input[@id=&quot;everyCalendar&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Parent iframe/Parent_mainframe</value>
   </webElementProperties>
</WebElementEntity>
